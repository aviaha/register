import { Component, OnInit , Input, Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();
  text;

  showTheButton = false;

  send(){
    this.myButtonClicked.emit(this.text);
  }
  showButton(){
    this.showTheButton =true;
  }
  hideButton(){
    this.showTheButton =false;

  }
  constructor() { }

  ngOnInit() {
    this.text=this.data.text;

  }
  
}
