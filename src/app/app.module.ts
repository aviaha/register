import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';




//מטריאל דיזיין
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input'; 
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; 




//Firebase modules 
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';


import { AppComponent } from './app.component';
import { TodosComponent } from './todos/todos.component';
import { TodoComponent } from './todo/todo.component';
import { RegistrationComponent } from './registration/registration.component';
import { CodesComponent } from './codes/codes.component';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { MainComponent } from './main/main.component';
import { Routes,RouterModule } from '@angular/router';

import{environment} from '../environments/environment';
import { UsertodosComponent } from './usertodos/usertodos.component';

@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    TodoComponent,
    RegistrationComponent,
    CodesComponent,
    LoginComponent,
    NavComponent,
    MainComponent,
    UsertodosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatInputModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
      RouterModule.forRoot([//הגדרת הראוטים
        {path:'',component:TodosComponent},
        {path:'register',component:RegistrationComponent},
        {path:'login',component:LoginComponent},
        {path:'codes',component:CodesComponent},
        {path:'users',component:UsertodosComponent},
        {path:'**',component:TodosComponent}

  
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
