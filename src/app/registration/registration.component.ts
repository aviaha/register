import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  email= 'rag';
  password:string;

  constructor(private auth:AuthService) { }

  ngOnInit() {
  }

  toregister(){
    this.auth.toregister(this.email, this.password)
  }

}
