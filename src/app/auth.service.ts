import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {Observable} from 'rxjs'
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  toregister(email:string, password:string){
    this.fireBaseAuth.auth.createUserWithEmailAndPassword(email, password)
  }

  constructor(private fireBaseAuth:AngularFireAuth, 
              private db:AngularFireDatabase) { }
}

