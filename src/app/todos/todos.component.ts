import { Component, OnInit } from '@angular/core';
import{AngularFireDatabase, AngularFireList} from '@angular/fire/database'

@Component({
  selector: 'todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  
  todoTextFromTodo= 'No text';
  showText(e){
    this.todoTextFromTodo = e;
  }

  todos = [];

  constructor(private db:AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos =[];
        todos.forEach(
          todo =>{
            let y = todo.payload.toJSON();
            y['$key'] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )
  }

}
